﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Final_JsonRpc_Server
{
    public class TravelPlannerService
    {
        TravelsEntities ctx = new TravelsEntities();
        SqlCommand cmd = new SqlCommand();
        List<string> travelsList = new List<string>();
        public void AddTravel(string name, string passport, double cost)
        {
            try
            {

                ctx.Travels.Add(new Travel() { Name = name, Passport = passport, Cost = (decimal)cost });
                ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                System.Console.WriteLine("Error saving record:\n" + ex.Message);
            }
        }
        public String[] GetAllTravels()
        {
            List<Travel> items = (from o in ctx.Travels select o).ToList();
            //3: Jerry (AB123456) $56.45
            foreach (Travel travel in items)
            {
                string t = string.Format("{0}: {1}({2}) ${3}", travel.Id, travel.Name, travel.Passport, travel.Cost);
                travelsList.Add(t);
            }
            return travelsList.ToArray();
        }
    }
}
