﻿using StreamJsonRpc;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Query.Dynamic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Final_JsonRpc_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            RefreshList();
        }
        async Task RefreshList()
        {
            using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
            {
                try
                {
                    await stream.ConnectAsync();
                    var jsonRpc = JsonRpc.Attach(stream);
                    string[] names = await jsonRpc.InvokeAsync<string[]>("GetAllTravels");
                    lvTravels.ItemsSource = names;
                    lvTravels.Items.Refresh();
                    tbName.Text = "";
                    tbCost.Text = "";
                    tbPassport.Text = "";

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Exception: " + ex);
                }
            }
        }
        private void tbName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name;
            string passport;
            double cost;

            using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
            {
                try
                {
                    stream.ConnectAsync();
                    var jsonRpc = JsonRpc.Attach(stream);
                    //CHECK NAME
                    name = tbName.Text;
                    if (name.Length < 2 || name.Length > 50)
                    {
                        MessageBox.Show( "Name Should be 2-50 char long", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    //CHECK PASSWORD
                    passport = tbPassport.Text;
                    var regex = new Regex(@"(^[A-Z]{2}\d{6}$)");
                    Match match = regex.Match(passport);
                    if (!match.Success)
                    {
                        MessageBox.Show("Passport should be 2 capital letters foloowed by 6 digits", "Error", MessageBoxButton.OK,MessageBoxImage.Error);
                        return;
                    }
                    //CHECK COST
                    if (!double.TryParse(tbCost.Text, out cost))
                    {
                        throw new FormatException();
                    }
                    if (cost < 0 || cost > 1000000)
                    {
                        MessageBox.Show( "Cost should be between 0 and 1000000 ", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    jsonRpc.InvokeAsync<string>("AddTravel", tbName.Text, tbPassport.Text, Convert.ToDouble(tbCost.Text));
                    RefreshList();
                }
                catch(FormatException ex)
                {
                    MessageBox.Show("Exception: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (MissingMethodException ex)
                {
                    MessageBox.Show("Exception: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
