﻿using StreamJsonRpc;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day03_Friends_StreamJsonRpc_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            refreshList();
        }
         async Task refreshList()
        {
            using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
            {
                await stream.ConnectAsync();
                var jsonRpc = JsonRpc.Attach(stream);
                string[] names = await jsonRpc.InvokeAsync<string[]>("GetAllFriends");
                lvFriends.ItemsSource = names;
                lvFriends.Items.Refresh();
                tbName.Text = "";
                int count = await jsonRpc.InvokeAsync<int>("GetFriendsCount");
                lblCount.Content="you have "+count.ToString()+" friends";
            }
        }
        private void ListView_SelectionChanged()
        {

        }

        private void  btAdd_Click(object sender, RoutedEventArgs e)
        {
            using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
            {
                stream.ConnectAsync();
                var jsonRpc = JsonRpc.Attach(stream);
               jsonRpc.InvokeAsync<string>("AddFriend",tbName.Text);
                refreshList();
            }
        }
    }
}
