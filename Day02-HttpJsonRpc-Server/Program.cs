﻿using HttpJsonRpc;
using System;
using System.Threading.Tasks;

namespace Day02_HttpJsonRpc_Server
{
    class Program
    {
        [JsonRpcClass("math")]
        public static class MathApi
        {
            [JsonRpcMethod]
            public static Task<int> Add(int n1, int n2)
            {
                var result = n1 + n2;

                return Task.FromResult(result);
            }
        }
        static void Main(string[] args)
        {
            JsonRpc.Start();

            Console.ReadLine();
        }
    }
}
