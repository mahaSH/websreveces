﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day02_HttpJsonRpc_Server
{
    interface IMathService
    {
        int Sum(int n1, int n2);
    }
}
