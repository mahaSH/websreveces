﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day02_HttpJsonRpc_Server
{
    class MathService : IMathService
    {
        public int Sum(int n1, int n2)
        {
            return n1 + n2;
        }
    }
}
