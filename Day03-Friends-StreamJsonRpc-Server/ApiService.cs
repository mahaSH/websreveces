﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03_Friends_StreamJsonRpc_Server
{

    class ApiService
    {
        FriendsDbContext ctx = new FriendsDbContext();
        SqlCommand cmd =new SqlCommand();
        List<string> freindsNames=new List<string>();
        public void AddFriend(string name) {
            try
            {
                
                ctx.Friends.Add(new Friend() { Name = name });
                ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                System.Console.WriteLine("Error saving record:\n" + ex.Message); 
            }
        }
       public string[] GetAllFriends() {

            var items = (from o in ctx.Friends select o.Name).ToList();
            foreach(string friend in items)
            {

                freindsNames.Add(friend);
            }
            return freindsNames.ToArray();
        }
        public int GetFriendsCount() {
           int count = (from o in ctx.Friends select o).Count();
            
            return count;
        } // returns count of records in the database
    }
}
