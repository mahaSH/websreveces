﻿using JsonRpc.Standard.Contracts;
using JsonRpc.Standard.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01HW_JSONRPC_Server
{
    class MathService : JsonRpcService
    {

        // The service instance is transcient. You cannot persist state in such a class.
        // So we need session.


        [JsonRpcMethod]
        public String add(int v1, int v2)
        {
            return "The sum is  " + (v1 + v2);
        }
    }
}
