﻿using JsonRpc.Standard.Contracts;
using JsonRpc.Standard.Server;
using JsonRpc.Streams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Day01HW_JSONRPC_Server
{
   
    class Program
    {
        private static IJsonRpcServiceHost BuildServiceHost()
        {
            var builder = new JsonRpcServiceHostBuilder
            {
                ContractResolver = new JsonRpcContractResolver
                {
                    // Use camelcase for RPC method names.
                    NamingStrategy = new CamelCaseJsonRpcNamingStrategy(),
                    // Use camelcase for the property names in parameter value objects
                    ParameterValueConverter = new CamelCaseJsonValueConverter()
                },
            };
            // Register all the services (public classes) found in the assembly
            builder.Register(typeof(Program).GetTypeInfo().Assembly);
            // Add a middleware to log the requests and responses
            builder.Intercept(async (context, next) =>
            {
                Console.WriteLine("> {0}", context.Request);
                await next();
                Console.WriteLine("< {0}", context.Response);
            });
            return builder.Build();
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            // Configure & build service host
            var host = BuildServiceHost();
            // Use MessageReader/MessageWriter to read and write messages.
            var serverHandler = new StreamRpcServerHandler((JsonRpc.Server.IJsonRpcServiceHost)host);
            // Though it's suggested that all feature types be interface types, for sake of
            // simplicity, here we just use a concrete class.
            //var session = new LibrarySessionFeature();
            //serverHandler.DefaultFeatures.Set(session);
            // Messages come from Console
            using (var reader = new ByLineTextMessageReader(Console.In))
            using (var writer = new ByLineTextMessageWriter(Console.Out))
            using (serverHandler.Attach(reader, writer))
            {
                // Wait for exit
               
            }
            Console.WriteLine("Server exited.");
        }
    }
}
