﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03_StreamJsonRCP_Server
{
    class ApiService
    {
        public int Add(int a, int b)
        {
            Console.WriteLine($"Received request: {a} + {b}");
            return a + b;
        }
    }
}
