﻿using JsonRpc.CoreCLR.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace Day02_Jsonrpc_Client_Final
{
    class Program
    {
        public static async Task<String> CallMathService()
        {
            try
            {
                Uri rpcEndpoint = new Uri("http://localhost:8080/Day02-JSORPC-Server-Marvin/MathServlet");
                JsonRpcWebClient rpc = new JsonRpcWebClient(rpcEndpoint);

                // you can use Json.Net JValue if the service returns a value or
                // JObject if it returns an object or you can provide your own
                // custom class type to be used when deserializing the rpc result
                var response = await rpc.InvokeAsync<JValue>("add", new int[] { 22, 54 });
                //   System.Console.WriteLine("RPC Reply:{0}", response.Result);
                return (string)response.Result;

            }
            catch (WebException ex)
            {
                Console.WriteLine("error making call");
                System.Console.WriteLine(ex.ToString());
                throw ex;
            }
            
        }
        static async Task Main(string[] args)
        {
            string results = await CallMathService();
            Console.WriteLine("results was:" + results);
            Console.WriteLine("press any key");
            Console.ReadKey();
        }
    }
}
