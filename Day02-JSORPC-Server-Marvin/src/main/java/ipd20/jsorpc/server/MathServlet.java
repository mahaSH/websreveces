/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd20.jsorpc.server;

import com.googlecode.jsonrpc4j.JsonRpcServer;
import ipd20.jsorpc.server.MathServece;
import ipd20.jsorpc.server.MathServeceImpl;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author conted
 */
public class MathServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MathServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MathServlet at " + request.getContextPath() + "</h1>");
            out.println("<p>Count of calls handled: " + callsCount + "</p>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    static int callsCount;
    private MathServece mathService;
    private JsonRpcServer jsonRpcServer;
        
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        callsCount++;
        jsonRpcServer.handle(request, response);
    }
    
    @Override
    public void init(ServletConfig config) {
        mathService = new MathServeceImpl();
        this.jsonRpcServer = new JsonRpcServer(mathService);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
