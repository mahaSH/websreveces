<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home.html.twig */
class __TwigTemplate_a7e766f6efae916ed73481172d08a2183f9330041317fb2d6099e223123a0197 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
            'scripts' => [$this, 'block_scripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\tMaha M.Shawkat
";
    }

    // line 7
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "\t        <meta charset=\"utf-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\" />
        <meta name=\"description\" content=\"\" />
        <meta name=\"author\" content=\"\" />
        <title>Resume - Start Bootstrap Theme</title>
        <link rel=\"icon\" type=\"image/x-icon\" href=\"assets/img/favicon.ico\" />
        <!-- Font Awesome icons (free version)-->
        <script src=\"https://use.fontawesome.com/releases/v5.13.0/js/all.js\" crossorigin=\"anonymous\"></script>
        <!-- Google fonts-->
        <link href=\"https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i\" rel=\"stylesheet\" type=\"text/css\" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href=\"styles/styles.css\" rel=\"stylesheet\" />
\t<!------ -->
";
    }

    // line 24
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "    <body id=\"page-top\">
        <!-- Navigation-->
        <nav class=\"navbar navbar-expand-lg navbar-dark bg-primary fixed-top \" id=\"sideNav\">
            <a class=\"navbar-brand js-scroll-trigger\" href=\"#page-top\">
\t\t\t<span class=\"d-block d-lg-none\">MAHA MAHMOOD SHAWKAT</span><span class=\"d-none d-lg-block \" ><img class=\"img-fluid img-profile rounded-circle mx-auto \" src=\"images/profile.jpg\" alt=\"Maha\" /></span></a
            ><button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><span class=\"navbar-toggler-icon\"></span></button>
            <div class=\"collapse navbar-collapse mt-5\" id=\"navbarSupportedContent\" >
                <ul class=\"navbar-nav\">
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#about\">About</a></li>
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#education\">Education</a></li>
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#skills\">Skills</a></li>
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#Aptitudes\">Aptitudes</a></li>
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#ImplementedProjects\">Impolemented Projects</a></li>
                </ul>
            </div>
        </nav>
        <!-- Page Content-->
        <div class=\"container-fluid p-0\">
            <!-- About-->
            
           
                <div class=\"resume-section-content fixed-top bg-white\" style=\"margin-left:175px; padding-top:100px;\">
                    <h1 class=\"mb-0\">Maha <span class=\"text-primary\">M. Shawkat</span></h1>
                    <div class=\"subheading mb-5\">Dollard-des-Ormeaux(QC) · (514) 443-5524 · <a href=\"mailto:name@email.com\">maha.baak@gmail.com</a></div>
                </div>
                 <section class=\"resume-section\" id=\"about\">
                <div class=\"resume-section-content\" >
                    <div class=\"subheading mb-5\" style=\"font-size:3rem; color:#bd5d38;\">ABOUT ME</div>
                    <p class=\"lead mb-5 large\" style=\"font-size:1.5rem;\">I am a junior web developer with a good background in computer structure and hardware.Ready to apply the skill sets I have learned so far like: information gathering, planning, designing, developing, and maintaing.</p>
                    <div class=\"social-icons\">
                        <a class=\"btn btn-secondary\" style=\"background:#bd5d38;\" href=\"http://www.linkedin.com/in/maha-m-shawkat-394a5319a\" target=\"_blank\">My LinkedIn</a><!--<a class=\"social-icon\" href=\"#\"><i class=\"fab fa-github\"></i></a><a class=\"social-icon\" href=\"www.linkedin.com/in/maha-m-shawkat-394a5319a\"><i class=\"fab fa-twitter\"></i></a><a class=\"social-icon\" href=\"#\"><i class=\"fab fa-facebook-f\"></i></a>-->
                        <a class=\"btn btn-secondary\" style=\"background:#bd5d38;\" href=\"/profile.pdf\" target=\"_blank\">My CV</a>
                        <a class=\"btn btn-secondary\" style=\"background:#bd5d38;\" href=\"https://bitbucket.org/mahaSH\" target=\"_blank\">My BITBUCKET</a>
                        </div>
                </div>
            </section>
            <hr class=\"m-0\" />            
            <!-- Education-->
            <section class=\"resume-section\" id=\"education\">
                <div class=\"resume-section-content\">
                    <h2 class=\"mb-5 subheading mb-5\" style=\"font-size:3rem; color:#bd5d38;\">Education</h2>
                    <div class=\"d-flex flex-column flex-md-row justify-content-between mb-5\">
                        
                        <img src=\"images/johnabbott.png\" height=\"50\" width=\"50\">
                        <div class=\"flex-grow-1 ml-2\">
                            <h3 class=\"mb-0\">John Abbott College</h3>
                            <div class=\"subheading mb-3\">internet Programming and Developement(AEC)</div>
                           
                        </div>
                        <div class=\"flex-shrink-0\"><span class=\"text-primary\">September 2019-September 2020</span></div>
                    </div>
                    <div class=\"d-flex flex-column flex-md-row justify-content-between\">
                     <img src=\"images/cisco.png\" height=\"50\" width=\"50\">
                        <div class=\"flex-grow-1 ml-2\">
                            <h3 class=\"mb-0\">CISCO Certificate</h3>
                            <div class=\"subheading mb-3\">CCNA1&CCNA2</div>
                        </div>
                        <div class=\"flex-shrink-0\"><span class=\"text-primary\">2007</span></div>
                    </div>
                    <br/><br/>
                     <div class=\"d-flex flex-column flex-md-row justify-content-between\">
                      <img src=\"images/mosul.jpg\" height=\"50\" width=\"50\">
                        <div class=\"flex-grow-1 ml-2\">
                            <h3 class=\"mb-0\">University of Mosul</h3>
                            <div class=\"subheading mb-3\">B.Sc. in Computer Engineering</div>
                        </div>
                        <div class=\"flex-shrink-0\"><span class=\"text-primary\">September 2001-June 2005</span></div>
                    </div>
                </div>
            </section>
            <hr class=\"m-0\" />
            <!-- Skills-->
            <section class=\"resume-section\" id=\"skills\">
                <div class=\"resume-section-content\" style=\"margin-top:165px\">
                    <h2 class=\"mb-5 subheading \" style=\"font-size:3rem; color:#bd5d38;\" >Skills</h2>
                    <div class=\"subheading mb-3\">Programming Languages</div>
                    <ul class=\"fa-ul mb-0 \" style=\"font-size:1.5em !important\">
                         <li class=\"mt-2\">
                            <img src=\"images/html.png\" height=\"40\" width=\"40\"class=\"mr-2\">HTML, CSS, Bootstrap, JavaScript, Jquery, Ajax, JSON<br/>
                        </li>
                         <li class=\"mt-3\">
                             <img src=\"images/java.png\" height=\"40\" width=\"40\" class=\"mr-2\">Java<br/>
                        </li>
                        <li class=\"mt-3\">
                            <img src=\"images/sql.png\" height=\"40\" width=\"40\"class=\"mr-2\">MSSQL, MySql, UML<br/>
                        </li>
                        <li class=\"mt-3\">
                             <img src=\"images/php.png\" height=\"40\" width=\"40\"class=\"mr-2\">PHP (meekrodb, slimframeworks), XAMPP(Apache, MySql)<br/>
                        </li>
                        <li class=\"mt-3\">
                             <img src=\"images/dotnet.jpg\" height=\"40\" width=\"40\"class=\"mr-2\"></i></span>C#, C# WPF, ASP.NET, MVC, Entity Framework<br/>
                        </li>
                    </ul>
                    <br/><br/>
                    <div class=\"subheading mb-3\">Tools</div>
                    <ul class=\"fa-ul mb-0\" style=\"font-size:1.5em !important\">
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Microsoft Visual Studio
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>NetBeans
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>BitBucket
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Trello
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Photoshop
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Microsoft SQL Server
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Visual Studio Code
                        </li>
                    </ul>
                </div>
            </section>
            <hr class=\"m-0\" />
            <!-- Interests-->
            <section class=\"resume-section\" id=\"Aptitudes\">
                <div class=\"resume-section-content\">
                    <h2 class=\"mb-5 subheading mb-5\" style=\"font-size:3rem; color:#bd5d38;\">Aptitudes:</h2>
                     <ul class=\"fa-ul mb-0 \" style=\"font-size:1.5em !important\">
                        <li >
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span><p class=\"font-weight-bold\">Logical Problem Solver:</p> \t
                            <ul>
                           <li> 
                            Believe there is always a solution
                           </li>
                            <li> 
                             Divides problems into smaller parts for solving
                            </li>
                            <li> 
                             Understand limitations
                            </li>
                            </ul>
                        </li>
                        <li>
                            <span class=\"fa-li \"><i class=\"fas fa-check\"></i></span><p class=\"font-weight-bold\">Patient:</p>
                            <ul> 
                            <li> 
                             Open-minded when it comes to changes.
                            </li>
                            <li> Able to take criticism and learn from it
                            </li>
                            <li> 
                             Tolerant to differences amongst team members
                            </li>
                            </ul>
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span><p class=\"font-weight-bold\">Punctual: </p>
                            <ul>
                         <li> 
                             Respect timelines and deadlines.
                            </li>
                             <li> 
                             Good Time Management Skills
                            </li>
                            </ul>
                        </li>
                        
                    </ul>
                </div>
            </section>
            <hr class=\"m-0\" />
            <!-- Awards-->
            <section class=\"resume-section\" id=\"ImplementedProjects\" >
                <div class=\"resume-section-content  \" style=\"margin-top:165px\">
                    <h2 class=\"mb-5 subheading \" style=\"font-size:3rem; color:#bd5d38;\">Implemented Projects</h2>
                    <ul class=\"fa-ul mb-0\" style=\"font-size:1.5rem;>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-trophy text-warning\"></i></span><a href=\"https://pickmeal.ipd20.com/\">PICKMEAL</a>(PHP course project)
                        </li>
                        <br/>
                        <a style=\"margin-left:50px;\" href=\"https://pickmeal.ipd20.com/\"><img calss=\"float-right\" src=\"images/pickmeal.png\" height=\"200px\" width=\"200px\"></a>
                        <ul class=\"fa-ul mb-0\">
                        <p style=\"  text-decoration:underline;\">Responsibilities:</p>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Sign up/signIn/SignOut system.
                            </li>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Sign up/Login Via Facebook..
                            </li>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Forget email functionality.
                            </li>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Restaurant menu.</li>
                         <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Search for restaurant by Name/Category.
                            </li>
                             <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Add to cart.
                            </li>
                        </ul>
                        <br/><br/>
                <li>
                            <span class=\"fa-li\"><i class=\"fas fa-trophy text-warning\"></i></span><a href=\"#\">RECIPEBASKET</a>
                        </li>
                        <br/>
                        <img style=\"margin-left:50px;\" calss=\"float-right\" src=\"images/recipebasket.png\" height=\"200px\" width=\"200px\">
                        <ul class=\"fa-ul mb-0\">
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Sign up/signIn/SignOut system.
                            </li>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Add/Update/Delete and search for Recipe.
                            </li>
                            
                        </ul>
                        
                    </ul>
                </div>
            </section>
        </div>";
    }

    // line 243
    public function block_scripts($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  306 => 243,  84 => 25,  80 => 24,  62 => 8,  58 => 7,  53 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}
\tMaha M.Shawkat
{% endblock title %}

{% block head %}
\t        <meta charset=\"utf-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\" />
        <meta name=\"description\" content=\"\" />
        <meta name=\"author\" content=\"\" />
        <title>Resume - Start Bootstrap Theme</title>
        <link rel=\"icon\" type=\"image/x-icon\" href=\"assets/img/favicon.ico\" />
        <!-- Font Awesome icons (free version)-->
        <script src=\"https://use.fontawesome.com/releases/v5.13.0/js/all.js\" crossorigin=\"anonymous\"></script>
        <!-- Google fonts-->
        <link href=\"https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700\" rel=\"stylesheet\" type=\"text/css\" />
        <link href=\"https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i\" rel=\"stylesheet\" type=\"text/css\" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href=\"styles/styles.css\" rel=\"stylesheet\" />
\t<!------ -->
{% endblock %}

{% block content %}
    <body id=\"page-top\">
        <!-- Navigation-->
        <nav class=\"navbar navbar-expand-lg navbar-dark bg-primary fixed-top \" id=\"sideNav\">
            <a class=\"navbar-brand js-scroll-trigger\" href=\"#page-top\">
\t\t\t<span class=\"d-block d-lg-none\">MAHA MAHMOOD SHAWKAT</span><span class=\"d-none d-lg-block \" ><img class=\"img-fluid img-profile rounded-circle mx-auto \" src=\"images/profile.jpg\" alt=\"Maha\" /></span></a
            ><button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><span class=\"navbar-toggler-icon\"></span></button>
            <div class=\"collapse navbar-collapse mt-5\" id=\"navbarSupportedContent\" >
                <ul class=\"navbar-nav\">
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#about\">About</a></li>
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#education\">Education</a></li>
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#skills\">Skills</a></li>
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#Aptitudes\">Aptitudes</a></li>
                    <li class=\"nav-item\"><a class=\"nav-link js-scroll-trigger\" href=\"#ImplementedProjects\">Impolemented Projects</a></li>
                </ul>
            </div>
        </nav>
        <!-- Page Content-->
        <div class=\"container-fluid p-0\">
            <!-- About-->
            
           
                <div class=\"resume-section-content fixed-top bg-white\" style=\"margin-left:175px; padding-top:100px;\">
                    <h1 class=\"mb-0\">Maha <span class=\"text-primary\">M. Shawkat</span></h1>
                    <div class=\"subheading mb-5\">Dollard-des-Ormeaux(QC) · (514) 443-5524 · <a href=\"mailto:name@email.com\">maha.baak@gmail.com</a></div>
                </div>
                 <section class=\"resume-section\" id=\"about\">
                <div class=\"resume-section-content\" >
                    <div class=\"subheading mb-5\" style=\"font-size:3rem; color:#bd5d38;\">ABOUT ME</div>
                    <p class=\"lead mb-5 large\" style=\"font-size:1.5rem;\">I am a junior web developer with a good background in computer structure and hardware.Ready to apply the skill sets I have learned so far like: information gathering, planning, designing, developing, and maintaing.</p>
                    <div class=\"social-icons\">
                        <a class=\"btn btn-secondary\" style=\"background:#bd5d38;\" href=\"http://www.linkedin.com/in/maha-m-shawkat-394a5319a\" target=\"_blank\">My LinkedIn</a><!--<a class=\"social-icon\" href=\"#\"><i class=\"fab fa-github\"></i></a><a class=\"social-icon\" href=\"www.linkedin.com/in/maha-m-shawkat-394a5319a\"><i class=\"fab fa-twitter\"></i></a><a class=\"social-icon\" href=\"#\"><i class=\"fab fa-facebook-f\"></i></a>-->
                        <a class=\"btn btn-secondary\" style=\"background:#bd5d38;\" href=\"/profile.pdf\" target=\"_blank\">My CV</a>
                        <a class=\"btn btn-secondary\" style=\"background:#bd5d38;\" href=\"https://bitbucket.org/mahaSH\" target=\"_blank\">My BITBUCKET</a>
                        </div>
                </div>
            </section>
            <hr class=\"m-0\" />            
            <!-- Education-->
            <section class=\"resume-section\" id=\"education\">
                <div class=\"resume-section-content\">
                    <h2 class=\"mb-5 subheading mb-5\" style=\"font-size:3rem; color:#bd5d38;\">Education</h2>
                    <div class=\"d-flex flex-column flex-md-row justify-content-between mb-5\">
                        
                        <img src=\"images/johnabbott.png\" height=\"50\" width=\"50\">
                        <div class=\"flex-grow-1 ml-2\">
                            <h3 class=\"mb-0\">John Abbott College</h3>
                            <div class=\"subheading mb-3\">internet Programming and Developement(AEC)</div>
                           
                        </div>
                        <div class=\"flex-shrink-0\"><span class=\"text-primary\">September 2019-September 2020</span></div>
                    </div>
                    <div class=\"d-flex flex-column flex-md-row justify-content-between\">
                     <img src=\"images/cisco.png\" height=\"50\" width=\"50\">
                        <div class=\"flex-grow-1 ml-2\">
                            <h3 class=\"mb-0\">CISCO Certificate</h3>
                            <div class=\"subheading mb-3\">CCNA1&CCNA2</div>
                        </div>
                        <div class=\"flex-shrink-0\"><span class=\"text-primary\">2007</span></div>
                    </div>
                    <br/><br/>
                     <div class=\"d-flex flex-column flex-md-row justify-content-between\">
                      <img src=\"images/mosul.jpg\" height=\"50\" width=\"50\">
                        <div class=\"flex-grow-1 ml-2\">
                            <h3 class=\"mb-0\">University of Mosul</h3>
                            <div class=\"subheading mb-3\">B.Sc. in Computer Engineering</div>
                        </div>
                        <div class=\"flex-shrink-0\"><span class=\"text-primary\">September 2001-June 2005</span></div>
                    </div>
                </div>
            </section>
            <hr class=\"m-0\" />
            <!-- Skills-->
            <section class=\"resume-section\" id=\"skills\">
                <div class=\"resume-section-content\" style=\"margin-top:165px\">
                    <h2 class=\"mb-5 subheading \" style=\"font-size:3rem; color:#bd5d38;\" >Skills</h2>
                    <div class=\"subheading mb-3\">Programming Languages</div>
                    <ul class=\"fa-ul mb-0 \" style=\"font-size:1.5em !important\">
                         <li class=\"mt-2\">
                            <img src=\"images/html.png\" height=\"40\" width=\"40\"class=\"mr-2\">HTML, CSS, Bootstrap, JavaScript, Jquery, Ajax, JSON<br/>
                        </li>
                         <li class=\"mt-3\">
                             <img src=\"images/java.png\" height=\"40\" width=\"40\" class=\"mr-2\">Java<br/>
                        </li>
                        <li class=\"mt-3\">
                            <img src=\"images/sql.png\" height=\"40\" width=\"40\"class=\"mr-2\">MSSQL, MySql, UML<br/>
                        </li>
                        <li class=\"mt-3\">
                             <img src=\"images/php.png\" height=\"40\" width=\"40\"class=\"mr-2\">PHP (meekrodb, slimframeworks), XAMPP(Apache, MySql)<br/>
                        </li>
                        <li class=\"mt-3\">
                             <img src=\"images/dotnet.jpg\" height=\"40\" width=\"40\"class=\"mr-2\"></i></span>C#, C# WPF, ASP.NET, MVC, Entity Framework<br/>
                        </li>
                    </ul>
                    <br/><br/>
                    <div class=\"subheading mb-3\">Tools</div>
                    <ul class=\"fa-ul mb-0\" style=\"font-size:1.5em !important\">
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Microsoft Visual Studio
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>NetBeans
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>BitBucket
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Trello
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Photoshop
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Microsoft SQL Server
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Visual Studio Code
                        </li>
                    </ul>
                </div>
            </section>
            <hr class=\"m-0\" />
            <!-- Interests-->
            <section class=\"resume-section\" id=\"Aptitudes\">
                <div class=\"resume-section-content\">
                    <h2 class=\"mb-5 subheading mb-5\" style=\"font-size:3rem; color:#bd5d38;\">Aptitudes:</h2>
                     <ul class=\"fa-ul mb-0 \" style=\"font-size:1.5em !important\">
                        <li >
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span><p class=\"font-weight-bold\">Logical Problem Solver:</p> \t
                            <ul>
                           <li> 
                            Believe there is always a solution
                           </li>
                            <li> 
                             Divides problems into smaller parts for solving
                            </li>
                            <li> 
                             Understand limitations
                            </li>
                            </ul>
                        </li>
                        <li>
                            <span class=\"fa-li \"><i class=\"fas fa-check\"></i></span><p class=\"font-weight-bold\">Patient:</p>
                            <ul> 
                            <li> 
                             Open-minded when it comes to changes.
                            </li>
                            <li> Able to take criticism and learn from it
                            </li>
                            <li> 
                             Tolerant to differences amongst team members
                            </li>
                            </ul>
                        </li>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span><p class=\"font-weight-bold\">Punctual: </p>
                            <ul>
                         <li> 
                             Respect timelines and deadlines.
                            </li>
                             <li> 
                             Good Time Management Skills
                            </li>
                            </ul>
                        </li>
                        
                    </ul>
                </div>
            </section>
            <hr class=\"m-0\" />
            <!-- Awards-->
            <section class=\"resume-section\" id=\"ImplementedProjects\" >
                <div class=\"resume-section-content  \" style=\"margin-top:165px\">
                    <h2 class=\"mb-5 subheading \" style=\"font-size:3rem; color:#bd5d38;\">Implemented Projects</h2>
                    <ul class=\"fa-ul mb-0\" style=\"font-size:1.5rem;>
                        <li>
                            <span class=\"fa-li\"><i class=\"fas fa-trophy text-warning\"></i></span><a href=\"https://pickmeal.ipd20.com/\">PICKMEAL</a>(PHP course project)
                        </li>
                        <br/>
                        <a style=\"margin-left:50px;\" href=\"https://pickmeal.ipd20.com/\"><img calss=\"float-right\" src=\"images/pickmeal.png\" height=\"200px\" width=\"200px\"></a>
                        <ul class=\"fa-ul mb-0\">
                        <p style=\"  text-decoration:underline;\">Responsibilities:</p>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Sign up/signIn/SignOut system.
                            </li>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Sign up/Login Via Facebook..
                            </li>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Forget email functionality.
                            </li>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Restaurant menu.</li>
                         <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Search for restaurant by Name/Category.
                            </li>
                             <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Add to cart.
                            </li>
                        </ul>
                        <br/><br/>
                <li>
                            <span class=\"fa-li\"><i class=\"fas fa-trophy text-warning\"></i></span><a href=\"#\">RECIPEBASKET</a>
                        </li>
                        <br/>
                        <img style=\"margin-left:50px;\" calss=\"float-right\" src=\"images/recipebasket.png\" height=\"200px\" width=\"200px\">
                        <ul class=\"fa-ul mb-0\">
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Sign up/signIn/SignOut system.
                            </li>
                            <li>
                                <span class=\"fa-li\"><i class=\"fas fa-check\"></i></span>Add/Update/Delete and search for Recipe.
                            </li>
                            
                        </ul>
                        
                    </ul>
                </div>
            </section>
        </div>{% endblock content %}{% block scripts %}{% endblock scripts %}
", "home.html.twig", "C:\\xampp\\htdocs\\ipd20\\maha\\templates\\home.html.twig");
    }
}
