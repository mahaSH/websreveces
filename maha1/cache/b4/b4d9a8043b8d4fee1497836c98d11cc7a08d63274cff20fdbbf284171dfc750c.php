<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_b5e37d6f3d875171550d3f1e2c3419a20e84a59615b65f0aef15fb5e848fa1fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'navbar' => [$this, 'block_navbar'],
            'content' => [$this, 'block_content'],
            'scripts' => [$this, 'block_scripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.0.8/css/all.css\">
        <script src=\"https://kit.fontawesome.com/62a1f178de.js\" crossorigin=\"anonymous\"></script>
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">
<link href=\"https://fonts.googleapis.com/css2?family=Jost:wght@500&family=Poppins&display=swap\" rel=\"stylesheet\">
       <link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">
<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js\"></script>
<script src=\"//code.jquery.com/jquery-1.11.1.min.js\"></script>
        <link rel=\"stylesheet\" href=\"styles/styles.css\">
        <link rel=\"icon\" type=\"image/png\" href=\"images/favicon.png\" />
        <title>";
        // line 13
        $this->displayBlock('title', $context, $blocks);
        echo " | Pickmeal</title>
        ";
        // line 14
        $this->displayBlock('head', $context, $blocks);
        // line 15
        echo "    </head>
   
        ";
        // line 17
        $this->displayBlock('navbar', $context, $blocks);
        // line 18
        echo "<!------ Include the above in your HEAD tag ---------->

 <!-- <body>
    <div id=\"container\">

        <div class=\"header\">

        </div>


        <div id=\"nav\">
            <ul>
 <li><a href=\"/Default.aspx\"\">Home</a></li>
 <li><a href=\"/webpages/Misc/contact.aspx\">Contact Us</a></li>      
                 <li><a href=\"/webpages/Misc/login.aspx\">Login</a></li>  
            </ul>  

        </div>


        <div class=\"sidebar\">

        </div>


        <div class=\"main\">
            <asp:ContentPlaceHolder ID=\"MainContent\" runat=\"server\">
            </asp:ContentPlaceHolder>
           

        </div>

    </div>

</body>-->
        ";
        // line 53
        $this->displayBlock('content', $context, $blocks);
        // line 54
        echo "        
        ";
        // line 55
        $this->displayBlock('scripts', $context, $blocks);
        // line 56
        echo "    </body>
</html>";
    }

    // line 13
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Order Restaurant Food Delivery Online & Take Out";
    }

    // line 14
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 17
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "        ";
    }

    // line 53
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 55
    public function block_scripts($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  144 => 55,  138 => 53,  134 => 18,  130 => 17,  124 => 14,  117 => 13,  112 => 56,  110 => 55,  107 => 54,  105 => 53,  68 => 18,  66 => 17,  62 => 15,  60 => 14,  56 => 13,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.0.8/css/all.css\">
        <script src=\"https://kit.fontawesome.com/62a1f178de.js\" crossorigin=\"anonymous\"></script>
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">
<link href=\"https://fonts.googleapis.com/css2?family=Jost:wght@500&family=Poppins&display=swap\" rel=\"stylesheet\">
       <link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">
<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js\"></script>
<script src=\"//code.jquery.com/jquery-1.11.1.min.js\"></script>
        <link rel=\"stylesheet\" href=\"styles/styles.css\">
        <link rel=\"icon\" type=\"image/png\" href=\"images/favicon.png\" />
        <title>{% block title %}Order Restaurant Food Delivery Online & Take Out{% endblock %} | Pickmeal</title>
        {% block head %}{% endblock %}
    </head>
   
        {% block navbar %}
        {% endblock navbar %}<!------ Include the above in your HEAD tag ---------->

 <!-- <body>
    <div id=\"container\">

        <div class=\"header\">

        </div>


        <div id=\"nav\">
            <ul>
 <li><a href=\"/Default.aspx\"\">Home</a></li>
 <li><a href=\"/webpages/Misc/contact.aspx\">Contact Us</a></li>      
                 <li><a href=\"/webpages/Misc/login.aspx\">Login</a></li>  
            </ul>  

        </div>


        <div class=\"sidebar\">

        </div>


        <div class=\"main\">
            <asp:ContentPlaceHolder ID=\"MainContent\" runat=\"server\">
            </asp:ContentPlaceHolder>
           

        </div>

    </div>

</body>-->
        {% block content %}{% endblock content %}
        
        {% block scripts %}{% endblock scripts %}
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\ipd20\\maha\\templates\\master.html.twig");
    }
}
